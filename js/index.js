const paths = document.querySelectorAll("#main-title path");

for (let index = 0; index < paths.length; index++) {
  const path = paths[index];
  const length = path.getTotalLength();
  path.style.strokeDasharray = length;
  path.style.strokeDashoffset = length;
}

new fullpage("#fullpage", {
  autoScrolling: true,
  navigation: true,
  anchors: ["home", "stack", "projects", "personal"],
  licenseKey: "YOUR_KEY_HERE"
});

const navSlide = () => {
  const nav = document.querySelector(".nav-links");
  const navLinks = document.querySelectorAll(".nav-links li");
  navLinks.forEach((link, index) => {
    link.style.animation = "navLinkFade 0.5s ease forwards";
  });
};

navSlide();
